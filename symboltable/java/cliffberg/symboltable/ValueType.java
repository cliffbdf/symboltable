package cliffberg.symboltable;

import cliffberg.utilities.AssertionUtilities;

import java.util.List;

/**
 Defines the expression value types that are permitted by the language.
 */
public interface ValueType {

	/**
	 Verify that this type can be assigned to the specified type.
	 */
	default void checkTypeAssignabilityTo(ValueType targetType) throws ConversionException {
		
		AssertionUtilities.assertThat(this.equals(targetType),
			"Type " + this.toString() + " is not assignable to " +
			targetType.toString());
	}
	
	/**
	 Verify that this type can be assigned from the specified type.
	 */
	default void checkTypeAssignabilityFrom(ValueType sourceType) throws ConversionException {
		
		AssertionUtilities.assertThat(this.equals(sourceType),
			"Type " + this.toString() + " is not assignable from " +
			sourceType.toString());
	}

	/**
	 Verify that this type can be assigned from the specified native Java type.
	 */
	void checkNativeTypeAssignabilityFrom(Class sourceNativeType) throws ConversionException;

	/**
	 Verify that a list of actual types can be assigned to the specified
	 declared types.
	 */
	default void checkTypeListAssignabilityTo(List<ValueType> sourceTypes,
		List<ValueType> targetTypes) throws ConversionException {
	
		AssertionUtilities.assertThat(sourceTypes.size() == targetTypes.size(),
			"Mismatch in number of source (" + sourceTypes.size() + ") and target (" +
			targetTypes.size() + ") arguments");
		
		int i = 0;
		for (ValueType targetType : targetTypes) {
			ValueType sourceType = sourceTypes.get(i++);
			sourceType.checkTypeAssignabilityTo(targetType);
		}
	}
}
