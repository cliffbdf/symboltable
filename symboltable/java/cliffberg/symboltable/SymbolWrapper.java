package cliffberg.symboltable;

public interface SymbolWrapper<Node> {
	String getText(Node node);
	int getLine(Node node);
	int getPos(Node node);
}
