package cliffberg.symboltable;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;
import java.io.PrintStream;

/**
 A table of the symbol (identifier) declarations made within a given lexical
 scope of a decl file.
 */
public class SymbolTable<Type extends ValueType, Node, TId extends Node>
	extends HashMap<String, List<SymbolEntry<Type, Node, TId>>>
{
	private String name; // may be null.
	private NameScope<Type, Node, TId> scope; // the lexical scope in which this table's sympbols are defined. May be null.
	private SymbolTable<Type, Node, TId> parent; // may be null (for env table).
	private List<SymbolTable<Type, Node, TId>> children; // may be null.
	private SymbolTable<Type, Node, TId> nextTable; // allows tables to be chained together.
	private SymbolWrapper<Node> symbolWrapper;
	
	public SymbolTable(NameScope scope, SymbolTable<Type, Node, TId> parent,
		List<SymbolTable<Type, Node, TId>> children, SymbolWrapper<Node> symbolWrapper)
	{
		this.scope = scope;
		this.parent = parent;
		this.symbolWrapper = symbolWrapper;
		if (children == null) this.children = new LinkedList<SymbolTable<Type, Node, TId>>();
		else this.children = children;
	}
	
	public SymbolTable(String name, NameScope scope, SymbolTable<Type, Node, TId> parent,
		List<SymbolTable<Type, Node, TId>> children, SymbolWrapper<Node> symbolWrapper)
	{
		this(scope, parent, children, symbolWrapper);
		this.name = name;
	}
	
	public NameScope<Type, Node, TId> getScope() { return scope; }
	
	public String getName()
	{
		if (name != null) return name;
		if (this.scope != null) {
			SymbolEntry selfEntry = this.scope.getSelfEntry();
			if (selfEntry != null) return selfEntry.getName();
		}
		return null; // an anonymous scope
	}
	
	public SymbolTable<Type, Node, TId> getParent() { return parent; }
	
	public List<SymbolTable<Type, Node, TId>> getChildren() { return children; }
	
	public void addChildTable(SymbolTable<Type, Node, TId> child)
	{
		children.add(child);
	}
	
	/**
	 Used to add a namespace to the current namespace. This usually only makes
	 sense for hierarchically nested namespaces - it generally does not make
	 sense for imported namespaces.
	 */
	public SymbolTable<Type, Node, TId> appendTable(SymbolTable<Type, Node, TId> table)
	{
		if (nextTable != null) {
			Node node = null;
			String line = "(unknown)";
			String text = "(unknown)";
			if (this.scope != null) {
				node = scope.getNodeThatDefinesScope();
				line = Integer.toString(symbolWrapper.getLine(node));
				text = symbolWrapper.getText(node);
			}
			throw new RuntimeException(
				"At \"" + text + "\", line " + line +
				", attempt to append a table to a table that already has an appended table (" +
				"named " + (nextTable.getName() == null? "<anonymous>" : nextTable.getName()) + ")");
		}
		this.nextTable = table;
		return table;
	}
	
	public SymbolTable<Type, Node, TId> getNextTable()
	{
		return nextTable;
	}
	
	/**
	 Return the entry, if any, for the specified symbol name. If there is more than one entry
	 for the name, throw an exception. Return the first name found - once a name is found,
	 search of child tables is abandoned.
	 */
	public SymbolEntry<Type, Node, TId> getEntry(String name)
	{
		List<SymbolEntry<Type, Node, TId>> entries = get(name);
		if (entries != null) {
			if (entries.size() > 1) throw new RuntimeException(
				"Expected a single entry in symbol table, but there are multiple");
			if (entries.size() == 0) return null;
			return entries.get(0);
		}
		if (nextTable == null) return null;
		
		// Look in next table (recursive).
		return nextTable.getEntry(name);
	}
	
	/**
	 Search this table and all child tables for the specified name, and return all instances.
	 */
	public List<SymbolEntry<Type, Node, TId>> getEntries(String name) {
		List<SymbolEntry<Type, Node, TId>> entries = new LinkedList<SymbolEntry<Type, Node, TId>>(this.get(name));
		if (nextTable == null) return entries;
		entries.addAll(nextTable.getEntries(name));
		return entries;
	}
	
	/**
	 Return a set of the entries that are defined in this symbol table. Do not
	 include entries of appended tables.
	 */
	public Set<List<SymbolEntry<Type, Node, TId>>> getEntries() {
		return new TreeSet<List<SymbolEntry<Type, Node, TId>>>(this.values());
	}
	
	/**
	 Return a set of the entries that are defined in this symbol table, including
	 entries of appended tables.
	 */
	public Set<SymbolEntry<Type, Node, TId>> getAllEntries() {
		
		Set<SymbolEntry<Type, Node, TId>> entries = new TreeSet<SymbolEntry<Type, Node, TId>>();
		SymbolTable<Type, Node, TId> table = this;
		for (;;) {
			Collection<List<SymbolEntry<Type, Node, TId>>> lists = table.values();
			for (List<SymbolEntry<Type, Node, TId>> listOfEntries : lists) {
				entries.addAll(listOfEntries);
			}
			table = table.getNextTable();
			if (table == null) break;
		}
		return entries;
	}
	
	public void addEntry(int curLineNo, String name, SymbolEntry<Type, Node, TId> entry,
		boolean allowOverloading)
	throws SymbolEntryPresent // if there is already an entry with name 'name'.
	{
		List<SymbolEntry<Type, Node, TId>> listOfEntries = get(name);
		if (listOfEntries == null) {
			listOfEntries = new LinkedList<SymbolEntry<Type, Node, TId>>();
			this.put(name, listOfEntries);
		}
		else {
			if ((! allowOverloading) && (listOfEntries.size() > 0)) {

				SymbolEntry<Type, Node, TId> firstEntry = listOfEntries.get(0);
			
				if (firstEntry instanceof DeclaredEntry) {
					Node definingNode = ((DeclaredEntry<Type, Node, TId>)firstEntry).getDefiningNode();
					int lineNo = this.symbolWrapper.getLine(definingNode);
					throw new SymbolEntryPresent(curLineNo, name, lineNo);
				}
				else
					throw new SymbolEntryPresent(curLineNo, name);
			}
		}
		
		listOfEntries.add(entry);
	}
	
	public NameScope getEnclosingScope(String name) throws SymbolNotPresent
	{
		List<SymbolEntry<Type, Node, TId>> entries = get(name);
		if (entries == null) throw new SymbolNotPresent(name);
		if (entries.size() == 0) throw new SymbolNotPresent(name);
		return entries.get(0).getEnclosingScope();
	}
	
	
	/**
	 Diagnostic only.
	 Print this symbol table, indented by 'level' tabs.
	 */
	public void print(PrintStream pr, int level)
	{
		String tabs = getTabs(level);
		if (scope == null) {
			pr.println("Cannot print symbol table: NameScope is null");
			return;
		}
		SymbolWrapper symbolWrapper = scope.getSymbolWrapper();
		Node definingNode = this.scope.getNodeThatDefinesScope();
		String lineInfo = "";
		if (definingNode == null) {
			System.err.println("****no defining node for name scope " + scope.getName());
		} else {
			int line = symbolWrapper.getLine(definingNode);
			lineInfo =  " (defined on line " + line + ")";
		}
		pr.println(tabs + "Table for name scope " + getScope().getName() + lineInfo + " --------");
		SymbolTable<Type, Node, TId> priorTable = null;
		for (SymbolTable<Type, Node, TId> table = this; table != null; table = table.getNextTable())
		{
			if (priorTable != null) {
				pr.print(tabs + "Appended table: ");
				if (table.getName() == null) pr.println("<anonymous>");
				else pr.println(table.getName());
			}
			for (String key : table.keySet())
			{
				List<SymbolEntry<Type, Node, TId>> entryList = table.get(key);
				if (entryList == null) continue;
				pr.println(tabs + "\t" + key);
				for (SymbolEntry<Type, Node, TId> childEntry : entryList) {
				
					String childLineInfo = "";
					if (childEntry instanceof DeclaredEntry) {
						int line = symbolWrapper.getLine(((DeclaredEntry)childEntry).getDefiningNode());
						lineInfo =  " (defined on line " + line + ")";
					}
					pr.println(tabs + "\t\t" + childEntry.getName() + lineInfo);
				}
			}
			priorTable = table;
		}
		pr.println(tabs + "----- end table " + this.getName());
	}
	
	private static String tabs = "";
	
	private static String getTabs(int level) {
		int len = tabs.length();
		for (int i = len+1; i <= level; i++) {
			tabs = tabs + '\t';
		}
		return tabs.substring(0, level);
	}
}
