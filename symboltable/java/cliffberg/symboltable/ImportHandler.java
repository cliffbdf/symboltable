package cliffberg.symboltable;

/**
 Finds the source code for a specified namespace, and then parses and
 analyzes that source (usually be delegating to a NamespaceProcessor).
 */
public interface ImportHandler {

	NameScope processNamespace(String namespacePath, CompilerState state, boolean parseOnly) throws Exception;
}
