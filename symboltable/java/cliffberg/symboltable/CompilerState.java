package cliffberg.symboltable;

import java.util.Hashtable;
import java.util.List;
import java.util.LinkedList;
import java.net.URLClassLoader;
import java.io.PrintStream;
import java.util.Iterator;
import static cliffberg.utilities.AssertionUtilities.*;

/**
 Contains the entire state of the compiler when processing an input file.
 When the compiler finishes processing a file, it returns the CompilerState.
 */
public class CompilerState<Type extends ValueType, Node, Start, TId extends Node>
{
	public CompilerState(int maxErrors, SymbolWrapper<Node> symbolWrapper) {
		this.maxNoOfErrors = maxNoOfErrors;
		this.symbolWrapper = symbolWrapper;
	}

	/**
	 The entry in the global scope that references the primary namespace.
	 */
	private NameScopeEntry<Type, Node, TId> primaryNamespaceSymbolEntry;

	/**
	 Root of the ASTs that are created. The first AST is for the main file;
	 others are for imported namespaces.
	 */
	private List<Start> asts = new LinkedList<Start>();

	/**
	 Scope in which the namespace is defined.
	 */
	private NameScope<Type, Node, TId> globalScope;

	/**
	 A stack of NameScopes that is maintained during the Analysis phase.
	 If Analysis completes without error, there will only be one NameScope
	 in the stack: the global scope.
	 */
	List<NameScope<Type, Node, TId>> scopeStack = new LinkedList<NameScope<Type, Node, TId>>();

	/**
	 A stack of scope names that have been entered, but which are not yet the
	 "current" scope, in terms of which part of the syntax tree has been descended.
	 E.g., when entering a production that defines a named region, the region
	 name can be pushed into the scope name stack, but the region, for name
	 resolution, might not be created until entering a child node of the region.
	 */
	private List<TId> pendingScopeNames = new LinkedList<TId>();

	private List<Node> pendingScopeDefiners = new LinkedList<Node>();

	/**
	 AST Node annotations that define names.
	 */
	private Hashtable<Node, DeclaredEntry<Type, Node, TId>> defs = new Hashtable<Node, DeclaredEntry<Type, Node, TId>>();

	/**
	 AST Node annotations that reference names.
	 */
	private Hashtable<Node, DeclaredEntry<Type, Node, TId>> refs = new Hashtable<Node, DeclaredEntry<Type, Node, TId>>();

	/**
	 AST Node annotations that hold statically determinable expression values.
	 */
	private Hashtable<Node, ExprAnnotation<Type, Node>> vals = new Hashtable<Node, ExprAnnotation<Type, Node>>();

	/**
	 AST annotations that hold a type descriptor for the node.
	 */
	private Hashtable<Node, TypeDescriptor<Type>> typeDescs = new Hashtable<Node, TypeDescriptor<Type>>();

	/**
	 AST Node annotations that define name scopes.
	 */
	private Hashtable<Node, NameScope<Type, Node, TId>> scopes = new Hashtable<Node, NameScope<Type, Node, TId>>();

	private List<ErrorMessage> errors = new LinkedList<ErrorMessage>();

	private List<WarningMessage> warnings = new LinkedList<WarningMessage>();

	private Iterator<ErrorMessage> errorIterator = null;

	private Iterator<WarningMessage> warningIterator = null;

	private int maxNoOfErrors;

	private int noOfErrors = 0;

	private int noOfWarnings = 0;

	private SymbolWrapper symbolWrapper;

	/*
	Methods
	*/

	public int getMaxNoOfErrors() { return maxNoOfErrors; }

	public List<Start> getASTs() { return this.asts; }

	public NameScope<Type, Node, TId> getGlobalScope() { return this.globalScope; }

	public void addLinePosExc(Node node, String msg) {
		addLinePosExc(node, msg, null);
	}

	public void addLinePosExc(Node node, Exception ex) {
		addLinePosExc(node, ex.getMessage(), ex);
	}

	public void addLinePosExc(Node node, String msg, Exception ex) {
		errors.add(new ErrorMessage(node, msg, ex));
		this.noOfErrors++;
		if (this.noOfErrors > this.maxNoOfErrors) {
			throw new RuntimeException("Maximum errors exceeded");
		}
	}

	public void throwLinePosExc(Node node, String msg) {
		throwLinePosExc(node, new RuntimeException(msg));
	}

	public void throwLinePosExc(Node node, Exception ex) {
		throwLinePosExc(node, ex.getMessage(), ex);
	}

	public void throwLinePosExc(Node node, String msg, Exception ex) {
		String m = "Error: " + createLinePosMsg(node, msg);
		if (ex == null)
			throw new LinePosException(m);
		else
			throw new LinePosException(m, ex);
	}

	public void addLinePosWarn(Node node, String msg) {
		warnings.add(new WarningMessage(node, msg));
		this.noOfWarnings++;
	}

	public ErrorMessage getNextErrorMessage() {
		if (this.errorIterator == null) this.errorIterator = errors.iterator();
		if (this.errorIterator.hasNext()) return this.errorIterator.next();
		else return null;
	}

	public WarningMessage getNextWarningMessage() {
		if (this.warningIterator == null) this.warningIterator = warnings.iterator();
		if (this.warningIterator.hasNext()) return this.warningIterator.next();
		else return null;
	}

	protected String createLinePosMsg(Node node, String msg) {
		return "At line " + this.symbolWrapper.getLine(node) + ", " + msg;
	}

	/**
	 Set the specified annotation ("a") as the "definition" attribute
	 for the specified node.
	 */
	void setDef(Node node, DeclaredEntry<Type, Node, TId> a)
	{
		if (getDef(node) != null) throw new RuntimeException(
			"Attempt to replace an Attribute");
		if(a == null) this.defs.remove(node);
		else this.defs.put(node, a);
	}

	public DeclaredEntry<Type, Node, TId> getDef(Node node) { return this.defs.get(node); }

	/**
	 Set the specified annotation ("a") as the "reference" attribute
	 for the specified node.
	 */
	void setRef(Node node, DeclaredEntry<Type, Node, TId> a)
	{
		if (getRef(node) != null) throw new RuntimeException(
			"Attempt to replace an existing Attribute " + getRef(node).getClass().getName() +
			" with a " + a.getClass().getName() + ", on a " + node.getClass().getName());
		if(a == null) this.refs.remove(node);
		else this.refs.put(node, a);
	}

	public DeclaredEntry<Type, Node, TId> getRef(Node node) { return this.refs.get(node); }

	/**
	 Set the specified annotation ("a") as the "value" attribute
	 for the specified node.
	 */
	public void setVal(Node node, ExprAnnotation<Type, Node> a) {
		if (getVal(node) != null) throw new RuntimeException(
			"Attempt to replace an existing Attribute " + getVal(node).getClass().getName() +
			" with a " + a.getClass().getName() + ", on a " + node.getClass().getName());
		if (a == null) this.vals.remove(node);
		else this.vals.put(node, a);
	}

	/**
	 Convenience method: creates an ExprAnnotation and uses it to set the node's expr attribute.
	 */
	public void setVal(Node node, Object value, TypeDescriptor typeDesc) {
		ExprAnnotation exprAnn = new ExprAnnotation(node, value, typeDesc);
		this.setVal(node, exprAnn);
	}

	public ExprAnnotation<Type, Node> getVal(Node node) { return vals.get(node); }

	/**
	 Set the specified annotation ("typeDesc") as the "typeDesc" attribute
	 for the specified node.
	 */
	public void setTypeDesc(Node node, TypeDescriptor<Type> typeDesc) {
		if (getTypeDesc(node) != null) throw new RuntimeException(
			"Attempt to replace an existing type desc attribute " + getTypeDesc(node).getClass().getName() +
			" with a " + typeDesc.getClass().getName() + ", on a " + node.getClass().getName());
		if (typeDesc == null) this.typeDescs.remove(node);
		else this.typeDescs.put(node, typeDesc);
	}

	public TypeDescriptor<Type> getTypeDesc(Node node) { return typeDescs.get(node); }

	/**
	 Set the specified annotation ("a") as the "name scope" attribute
	 for the specified node.
	 */
	public void setScope(Node node, NameScope<Type, Node, TId> a) {
		if (getScope(node) != null) throw new RuntimeException(
			"Attempt to replace an existing Attribute " + getScope(node).getClass().getName() +
			" with a " + a.getClass().getName() + ", on a " + node.getClass().getName());
		if (a == null) this.scopes.remove(node);
		else this.scopes.put(node, a);
	}

	public NameScope<Type, Node, TId> getScope(Node node) { return scopes.get(node); }

	public NameScopeEntry getPrimaryNamespaceSymbolEntry() { return primaryNamespaceSymbolEntry; }

	void setPrimaryNamespaceSymbolEntry(NameScopeEntry e) {
		primaryNamespaceSymbolEntry = e;
	}

	public SymbolEntry getIdBinding(TId id) {
		return (SymbolEntry)(this.getDef(id));
	}

	public void setGlobalScope(NameScope<Type, Node, TId> scope) { this.globalScope = scope; }

	protected void pushNameScope(NameScope<Type, Node, TId> scope) {
		System.err.println("Pushing NameScope " + scope.getName());  // debug
		//(new Exception("pushing NameScope " + scope.getName())).printStackTrace();
		this.scopeStack.add(0, scope);
	}

	protected NameScope<Type, Node, TId> popNameScope() {
		System.err.println("Popping NameScope " + scopeStack.get(0).getName());  // debug
		//(new Exception("popping NameScope " + scopeStack.get(0).getName())).printStackTrace();
		return this.scopeStack.remove(0);
	}

	/**
	 An element may be null, indicating an anonymous scope.
	 */
	void pushScopeName(TId name, Node nodeThatDefinesScope) {
		System.err.println("Pushing scope name " + (name == null? "null" : // debug
			getSymbolWrapper().getText(name)));  // debug
		this.pendingScopeNames.add(0, name);
		this.pendingScopeDefiners.add(0, nodeThatDefinesScope);
	}

	TId popScopeName() {
		assertThat(this.pendingScopeDefiners.size() > 0, "No scope definer to pop");
		assertThat(this.pendingScopeNames.size() > 0, "No scope name to pop");
		System.err.println("Popping scope name " + this.pendingScopeNames.get(0));  // debug
		//(new RuntimeException("Popping scope name " + this.pendingScopeNames.get(0))).printStackTrace();  // debug
		this.pendingScopeDefiners.remove(0);
		return this.pendingScopeNames.remove(0);
	}

	public TId getPendingScopeName() {
		if (this.pendingScopeNames.size() == 0) return null;
		return this.pendingScopeNames.get(0);
	}

	public Node getPendingScopeDefiner() {
		if (this.pendingScopeNames.size() == 0) return null;
		return this.pendingScopeDefiners.get(0);
	}

	/**
	 Switch the current scope stack with the specified one, and return the
	 original scope stack.
	 */
	List<NameScope<Type, Node, TId>> swapScopeStack(List<NameScope<Type, Node, TId>> newScopeStack) {
		List<NameScope<Type, Node, TId>> originalScopeStack = scopeStack;
		this.scopeStack = newScopeStack;
		return originalScopeStack;
	}

	public ExprAnnotation setExprAnnotation(Node node, Object value, TypeDescriptor typeDesc)
	{
		ExprAnnotation annotation = new ExprAnnotation(node, value, typeDesc);
		this.setVal(node, annotation);
		return annotation;
	}

	public ExprAnnotation getExprAnnotation(Node node) {
		return (ExprAnnotation)(this.getVal(node));
	}

	public SymbolWrapper getSymbolWrapper() { return this.symbolWrapper; }

	/*
	Nested classes
	*/

	public abstract class Message {

		protected Node node;
		protected String msg;

		Message(Node node, String msg) {
			this.node = node;
			this.msg = msg;
		}
	}

	public class ErrorMessage extends Message {

		private Exception ex;

		ErrorMessage(Node node, String msg, Exception ex) {
			super(node, msg);
			this.ex = ex;
		}

		public void print(PrintStream ps) {
			String msg = "Error: " + createLinePosMsg(this.node, this.msg +
				(ex == null ? "" : "; " + ex.getMessage()));
			ps.println(msg);
		}
	}

	public class WarningMessage extends Message {

		WarningMessage(Node node, String msg) {
			super(node, msg);
		}

		public void print(PrintStream ps) {
			String msg = "Warning: " + createLinePosMsg(this.node, this.msg);
			ps.println(msg);
		}
	}
}
