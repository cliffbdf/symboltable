package cliffberg.symboltable;

import java.util.List;
import java.util.LinkedList;


/**
 A symbol table entry. Each entry defines a symbol (identifer). The symbol might be
 implicitly declared - i.e., not actually appearing in a declaration.
 */
public abstract class SymbolEntry<Type extends ValueType, Node, TId extends Node> implements Annotation
{
	private String name;
	private NameScope enclosingScope;
	private boolean declaredPublic = false;
	private TypeDescriptor<Type> typeDescriptor = null;
	private List<NameScope<Type, Node, TId>> implementedNameScopes = new LinkedList<NameScope<Type, Node, TId>>();

	public SymbolEntry(String name, NameScope enclosingScope)
	{
		this.name = name;
		if (name.equals("")) throw new RuntimeException("Empty name");
		this.enclosingScope = enclosingScope;  // the scope in which the id is defined.
	}

	public String getName() { return name; }

	public NameScope<Type, Node, TId> getEnclosingScope() { return enclosingScope; }

	public void setTypeDesc(TypeDescriptor<Type> td) { this.typeDescriptor = td; }

	public TypeDescriptor<Type> getTypeDesc() { return this.typeDescriptor; }

	public Type getType() throws UnknownTypeException {
		TypeDescriptor<Type> td = getTypeDesc();
		if (td == null) throw new UnknownTypeException();
		return td.getType();
	}

	/**
	 Add additional name scopes that the symbol implements, beyond any that it
	 defines itself.
	 */
	public void addImplementedNameScope(NameScope<Type, Node, TId> scope) {
		this.implementedNameScopes.add(scope);
	}

	public List<NameScope<Type, Node, TId>> getImplementedNameScopes() {
		return this.implementedNameScopes;
	}

	public boolean isDeclaredPublic() { return declaredPublic; }

	public void setDeclaredPublic() { declaredPublic = true; }

	public String toString()
	{
		return this.getClass().getName() + ": name=" + name + ", enclosingScope=" + enclosingScope.toString();
	}
}
