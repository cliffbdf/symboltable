package cliffberg.symboltable;

import java.io.File;
import java.io.Reader;
import java.io.FileReader;

/**
 Attempts to read the namespace from the file system, using the env
 variable pathEnvVarName.
 */
public class FileImportHandler extends ImportHandlerBase {

	private String pathEnvVarName;
	
	public FileImportHandler(AnalyzerFactory analyzerFactory, String pathEnvVarName) {
		super(analyzerFactory);
		this.pathEnvVarName = pathEnvVarName;
	}
	
	public NameScope processNamespace(String namespace, CompilerState state, boolean parseOnly) throws Exception {
		
		System.out.println("------------Importing namespace " + namespace); // debug
		
		// Obtain value of import path.
		String lookupPath = System.getenv(pathEnvVarName);
		if (lookupPath == null) {
			throw new RuntimeException("Env variable " + pathEnvVarName + " not set");
		}
		
		// Split the lookup path into its parts.
		String[] pathDirs = lookupPath.split(":");
		
		// Construct file path represented by the namespace path. I.e.,
		// convert periods to slashes.
		String relativeFilePath = namespace.replace('.', File.separatorChar);
		
		// Attempt to find a file with the specified path name.
		File namespaceFile = null;
		for (String dirStr : pathDirs) {
			File dir = new File(dirStr);
			if (! dir.isDirectory()) {
				throw new RuntimeException("File " + dir.toString() + " is not a directory");
			}
			
			File f = new File(dir, relativeFilePath);
			if (f.exists()) {  // found
				namespaceFile = f;
				break;
			}
		}
		if (namespaceFile == null) {
			throw new RuntimeException("Namespace " + namespace + " not found in the " + pathEnvVarName);
		}
		
		// Recursively call the compiler on the file.
		
		Reader reader;
		try {
			reader = new FileReader(namespaceFile);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		
		NamespaceProcessorBase namespaceProcessor = getAnalyzerFactory().createNamespaceProcessor();
		return namespaceProcessor.processNamespace(reader, parseOnly);
	}
}
