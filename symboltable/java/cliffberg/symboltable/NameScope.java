package cliffberg.symboltable;

import static cliffberg.utilities.AssertionUtilities.*;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Collection;
import java.io.PrintStream;

/**
 All Axxx classes that define a lexical scope should be annotated with this.
 A NameScope contains a SymbolTable.
 */
public class NameScope<Type extends ValueType, Node, TId extends Node> implements Annotation
{
	private Node nodeThatDefinesScope;
	private NameScope parentScope;  // may be null
	private SymbolTable<Type, Node, TId> symbolTable;
	private SymbolWrapper<Node> symbolWrapper;
	
	/**
	 the symbol table entry for this scope's name.
	 Note: The scopes that do not have a self entry are top-level scopes and
	 anonymous scopes.
	 */
	private NameScopeEntry selfEntry = null;
	
	private Map<String, List<IdentHandler<Node, TId>>> identHandlers = new HashMap<String, List<IdentHandler<Node, TId>>>();
	
	public NameScope(Node nodeThatDefinesScope, NameScope parentScope, SymbolWrapper<Node> symbolWrapper)
	{
		this(null, nodeThatDefinesScope, parentScope, symbolWrapper);
	}
	
	public NameScope(String name, Node nodeThatDefinesScope, NameScope parentScope,
		SymbolWrapper<Node> symbolWrapper)
	{
		if (name != null) {
			if ((! name.equals("Global")) && (parentScope == null)) {
				System.out.println("**************************");
				throw new RuntimeException("**************************");
			}
		}
		this.nodeThatDefinesScope = nodeThatDefinesScope;
		this.parentScope = parentScope;
		this.symbolWrapper = symbolWrapper;
		if (symbolWrapper == null) throw new RuntimeException("SymbolWrapper is null");
		SymbolTable parentTable = (parentScope == null ? null : parentScope.getSymbolTable());
		this.symbolTable = new SymbolTable<Type, Node, TId>(name, this, parentTable, null, symbolWrapper);
		if (parentTable != null) parentTable.addChildTable(this.symbolTable);
	}
	
	/**
	 Returned Node is one of the Node types considered to be a 'NameScope'.
	 */
	public Node getNodeThatDefinesScope() { return nodeThatDefinesScope; }
	
	public NameScope<Type, Node, TId> getParentNameScope() { return parentScope; }
	
	public SymbolWrapper<Node> getSymbolWrapper() { return symbolWrapper; }
	
	public SymbolTable<Type, Node, TId> getSymbolTable() { return symbolTable; }
	
	public SymbolEntry getEntry(String name) { return symbolTable.getEntry(name); }
	
	public void addEntry(int curLineNo, String name, SymbolEntry entry, boolean allowOverloading)
	throws SymbolEntryPresent
	{
		symbolTable.addEntry(curLineNo, name, entry, allowOverloading);
	}
	
	public ValueType getType() throws UnknownTypeException { return getSelfEntry().getType(); }
	
	public TypeDescriptor<Type> getTypeDesc() { return getSelfEntry().getTypeDesc(); }
	
	public DeclaredEntry getDeclaredEntry(String name) {
		SymbolEntry entry = getEntry(name);
		if (entry == null) return null;
		assertThat(entry instanceof DeclaredEntry,
			"SymbolEntry is not a DeclaredEntry: it is a " + entry.getClass().getName());
		return (DeclaredEntry)entry;
	}	
	
	public String getName() { return symbolTable.getName(); }  // may be null
	
	/**
	 Save reference to the entry that parent scope's symbol table has for this
	 name scope. Thus, this is the symbol table entry for this scope's name.
	 Note: a namespace does not have a self-entry, since the self entry would
	 point outside the namespace (into the global symbol table), and we want the
	 namespace model to be self-contained.
	 */
	public NameScopeEntry setSelfEntry(NameScopeEntry entry)
	{
		return this.selfEntry = entry;
	}
	
	public NameScopeEntry getSelfEntry() { return selfEntry; }
	
	public void addIdentHandler(TId id, IdentHandler<Node, TId> handler)
	{
		String name = symbolWrapper.getText(id);
		List<IdentHandler<Node, TId>> handlersForName = identHandlers.get(name);
		if (handlersForName == null) {
			handler.setOriginal();  // signal that this is the first handler for name
			List<IdentHandler<Node, TId>> handlerForName = new LinkedList<IdentHandler<Node, TId>>();
			this.identHandlers.put(name, handlersForName);
		}
		handlersForName.add(handler);
	}
	
	public void removeIdentHandler(TId id) {
		String name = symbolWrapper.getText(id);
		List<IdentHandler<Node, TId>> handlersForName = identHandlers.get(name);
		handlersForName.remove(id);
	}
	
	public IdentHandler<Node, TId> getIdentHandler(TId id) {
		String name = symbolWrapper.getText(id);
		List<IdentHandler<Node, TId>> handlersForName = identHandlers.get(name);
		for (IdentHandler<Node, TId> handler : handlersForName) {
			if (handler.getId() == id) return handler;
		}
		return null;
	}
	
	public List<IdentHandler<Node, TId>> getHandlersForName(String name) {
		return identHandlers.get(name);
	}
	
	public Set<String> getHandlerNames() {
		return identHandlers.keySet();
	}
	
	/**
	 Iterate through this scope's delayed name resolution handlers, and attempt
	 to resolve the unresolved references.
	 */
	public void resolveForwardReferences(DeclaredEntry<Type, Node, TId> entry, VisibilityChecker checker)
	{
		List<IdentHandler<Node, TId>> handlersForName = identHandlers.get(entry.getName());
		if (handlersForName == null) return;
		for (IdentHandler<Node, TId> handler : handlersForName) {
			handler.checkForPathResolution(entry, checker);
		}
	}
	
	/**
	 Diagnostic method: print out the contents of this name scope.
	 */
	public void print(PrintStream pr, int level) {
		getSymbolTable().print(pr, level);
	}
	
	/**
	 Diagnostic method: print this name scope, and all enclosing name scopes,
	 beginning from this one and proceeding upward.
	 */
	public void printUpward(PrintStream pr) {
		printUpward(pr, 0);
	}
	
	protected void printUpward(PrintStream pr, int level) {
		pr.println("Name scopes:");
		for (NameScope scope = this; scope != null; scope = scope.getParentNameScope()) {
			scope.print(pr, level+1);
		}
	}
	
	/**
	 Print this name scope, and all child name scopes, recursively.
	 */
	public void printDownward(PrintStream pr) {
		printDownward(pr, 0);
	}
	
	protected void printDownward(PrintStream pr, int level) {
		print(pr, level);
		List<SymbolTable<Type, Node, TId>> childTables = getSymbolTable().getChildren();
		for (SymbolTable<Type, Node, TId> childTable : childTables) {
			NameScope childScope = childTable.getScope();
			childScope.printDownward(pr, level+1);
		}
	}
}
