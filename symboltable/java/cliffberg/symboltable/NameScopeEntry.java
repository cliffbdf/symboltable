package cliffberg.symboltable;

/**
 A DeclaredEntry that refers to a NameScope. I.e., the name of the entry is
 a named declarative region. NameScopeEntries must be created by the
 Analyzer whenever it encounters an Id that defines a scope. NameScopeEntries
 always refer to Ids.
 */
public class NameScopeEntry<Type extends ValueType, Node, TId extends Node> extends DeclaredEntry<Type, Node, TId>
{
	private NameScope<Type, Node, TId> ownedScope;
	
	public NameScopeEntry(NameScope<Type, Node, TId> ownedScope, String name, NameScope<Type, Node, TId> enclosingScope)
	{
		super(name, enclosingScope, ownedScope.getNodeThatDefinesScope());
		this.ownedScope = ownedScope;
	}
	
	public NameScope<Type, Node, TId> getOwnedScope() { return this.ownedScope; }
}
