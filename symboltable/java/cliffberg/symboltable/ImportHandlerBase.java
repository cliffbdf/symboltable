package cliffberg.symboltable;

import java.util.List;
import java.util.LinkedList;

public abstract class ImportHandlerBase implements ImportHandler {

	private AnalyzerFactory analyzerFactory;
	
	public ImportHandlerBase(AnalyzerFactory analyzerFactory) {
		this.analyzerFactory = analyzerFactory;
	}
	
	public abstract NameScope processNamespace(String namespacePath, CompilerState state, boolean parseOnly) throws Exception;
	
	/**
	 Locate the specified namespace, and parse and analyze it. Add its AST to
	 the CompilerState. Also add the new namespace to the global scope.
	 */
	public void processNamespace(String namespaceName, boolean parseOnly) throws Exception {
		
		CompilerState state = this.analyzerFactory.getCompilerState();
		
		// Replace NameScope stack with a fresh one.
		List<NameScope> originalScopeStack =
			state.swapScopeStack(new LinkedList<NameScope>());
		state.pushNameScope(state.getGlobalScope());
		
		// Import and process the namespace. The import handler uses a NamespaceProcessor
		// that was given to it at construction.
		NameScope importedScope = processNamespace(namespaceName, state, parseOnly);
		
		// Restore NameScope stack.
		state.swapScopeStack(originalScopeStack);
		
		// Add the new namespace to the global scope.
		state.getGlobalScope().getSymbolTable().appendTable(importedScope.getSymbolTable());
	}
	
	protected AnalyzerFactory getAnalyzerFactory() { return this.analyzerFactory; }
}
