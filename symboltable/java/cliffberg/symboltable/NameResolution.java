package cliffberg.symboltable;

import static cliffberg.utilities.PathUtilities.*;
import static cliffberg.utilities.AssertionUtilities.*;

import java.util.Hashtable;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

/**
 Add methods for name resolution, symbol table creation and management, and
 Node annotation. These methods are mostly language-agnostic.
 */
public abstract class NameResolution<Type extends ValueType, Node, Start,
	AOidRef extends Node, TId extends Node> //extends BaseListener
{
	private CompilerState<Type, Node, Start, TId> state;
	private List<IdentHandler> identHandlers = new LinkedList<IdentHandler>();
	private Map<TId, IdentSemanticHandler> identSemanticHandlers = new HashMap<TId, IdentSemanticHandler>();

	public NameResolution(CompilerState<Type, Node, Start, TId> state)
	{
		this.state = state;
	}

	public abstract ImportHandler getImportHandler();

	public abstract NameScopeEntry getEnclosingScopeEntry();

	public CompilerState<Type, Node, Start, TId> getState() { return state; }

	public SymbolWrapper<Node> getSymbolWrapper() { return this.state.getSymbolWrapper(); }

	public void addIdentHandler(IdentHandler handler) { identHandlers.add(handler); }

	public void removeIdentHandler(IdentHandler handler) { identHandlers.remove(handler); }

	public List<IdentHandler> getIdentHandlers() { return identHandlers; }


	/** Set and get Node attributes */

	public void setDef(Node node, DeclaredEntry<Type, Node, TId> a) {
		state.setDef(node, a);
	}

	public DeclaredEntry<Type, Node, TId> getDef(Node node) { return state.getDef(node); }

	public void setRef(Node node, DeclaredEntry<Type, Node, TId> a) {
		state.setRef(node, a);
	}

	public DeclaredEntry<Type, Node, TId> getRef(Node node) { return state.getRef(node); }

	public void setVal(Node node, ExprAnnotation<Type, Node> a) {
		state.setVal(node, a);
	}

	public ExprAnnotation<Type, Node> getVal(Node node) { return state.getVal(node); }

	public void setScope(Node node, NameScope<Type, Node, TId> a) {
		state.setScope(node, a);
	}

	public NameScope<Type, Node, TId> getScope(Node node) { return state.getScope(node); }


	/* Scope management */

	public NameScope<Type, Node, TId> getGlobalScope() { return state.getGlobalScope(); }

	public NameScope<Type, Node, TId> pushNameScope(NameScope scope)
	{
		state.pushNameScope(scope);
		return scope;
	}

	public NameScope<Type, Node, TId> popNameScope()
	{
		return state.popNameScope();
	}

	public NameScope<Type, Node, TId> getCurrentNameScope()
	{
		if (state.scopeStack.size() == 0) return null;

		List<NameScope<Type, Node, TId>> ss = state.scopeStack;
		NameScope ns = ss.get(0);
		return ss.get(0);
		//return state.scopeStack.get(0);
	}

	public void pushScopeName(TId name, Node nodeThatDefinesScope) {
		this.state.pushScopeName(name, nodeThatDefinesScope);
	}

	public TId popScopeName() {
		return this.state.popScopeName();
	}

	public TId getPendingScopeName() {
		return this.state.getPendingScopeName();
	}

	public Node getPendingScopeDefiner() {
		return this.state.getPendingScopeDefiner();
	}

	/**
	 Resolve a reference to a name that is declared elsewhere. See the algorithm
	 defined in the slide "Static Hierarchy Name Resolution" at
	 https://drive.google.com/open?id=1ckD59yy3aQssyUFzdOh0tbGC66pLmx1Q65vimY-HESc
	 @param resolveGlobally - if true, then the language requires that when a
	 name is encountered, and if that name has already been defined at any level
	 of the symbol table hierarchy, then the name reference is immediately bound
	 to the definition. If false, the reference will only be found if a definition
	 has been found at the current hierarchy level.
	 @param definingNode - may be nul. Only set if the name reference _might_
	 be a definition of the name, in which case definingNode is the non-terminal
	 representing the definition.
	 @return true if a binding was made; false if a delayed identifier resolution
	 handler was created and attached to the id.
	 */
	public boolean delayedRefRes(boolean resolveGlobally, Node definingNode, TId id,
		boolean couldServeAsDeclaration, final VisibilityChecker checker)
	{
		SymbolEntry<Type, Node, TId> entry = null;
		if (resolveGlobally) {
			// Find the declaration of the id. If it exists, annotate it.
			entry = identifySymbol(id, checker);
		} else {
			// Determine if the symbol has already been declared within the
			// current scope: if so, bind to that declaration.
			entry = identifySymbolLocally(id);
		}

		if (entry == null) {
			// Might be a reference to something that is declared later.
			//
			// Identify all the enclosing scopes, and attach a handler to each one.
			// The handler should be invoked whenever a new symbol is entered
			// into a scope. The handler should annotate the entry,
			// and then remove itself from all of the scopes to which it is attached.
			IdentHandler<Node, TId> handler = new IdentHandler<Node, TId>(this, this.getSymbolWrapper(),
				id,  // the symbol that needs to be resolved - it might be a
					// reference, or it might be a defining name
				definingNode, getCurrentNameScope()) {

				/** NameScope contains resolveForwardReferences, which calls each
					handler's checkForPathResolution, which in turn calls bindRetroactively
					followed by removeFromAllScopes:

					<NameScope>.resolveForwardReferences
						-> <IdentHandler>.checkForPathResolution
							-> <IdentHandler>.bindRetroactively
							-> <IdentHandler>.removeFromAllScopes
				*/
				public void bindRetroactively(DeclaredEntry entry) {
					NameResolution.this.bind(id, checker, entry);

					// If there is a semantic handler, execute it.
					IdentSemanticHandler h = getIdentSemanticHandler(id);
					if (h != null) h.semanticAction(entry);
				}
			};

			if (couldServeAsDeclaration) handler.setCouldServeAsDeclaration();
			return false;

		} else {  // declaration found.
			// Annotate the Id reference with the DeclaredEntry that defines the Id.
			if (! (entry instanceof DeclaredEntry)) throw new RuntimeException(
				"Unexpected: entry is a " + entry.getClass().getName());
			bind(id, checker, (DeclaredEntry)entry);
			return true;
		}
	}

	/**
	 Register the specified semantic handler for the specified node. The node must
	 be a symbol reference - specifically a AOidRef.
	 A semantic handlers for a symbol reference is invoked when the symbol reference
	 is matched with the symbol declaration. Only one semantic handler can be
	 registered for a given node.
	 */
	public void registerSemanticHandlerFor(TId id, IdentSemanticHandler handler) {

		assertThat(this.identSemanticHandlers.get(id) == null,
			"A semantic handler for node '" + getSymbolWrapper().getText(id) +
			"' is already registered");
		this.identSemanticHandlers.put(id, handler);
	}

	/**
	 Perform semantic actions when the specified symbol reference is evantually
	 declared. Semantic handlers are invoked when a declaration is recognized
	 and after the symbol reference has been matched up with the symbol declaration.
	 */
	public abstract class IdentSemanticHandler {
		private TId ref;

		public IdentSemanticHandler(TId ref) {
			this.ref = ref;
			registerSemanticHandlerFor(ref, this);
		}

		public abstract void semanticAction(DeclaredEntry entry);
	}

	/**
	 Return the semantic handler for the specified symbol reference, or null if
	 there is not one.
	 */
	public IdentSemanticHandler getIdentSemanticHandler(TId id) {

		return this.identSemanticHandlers.get(id);
	}

	/**
	 Create a declared entry for the specified terminal node.
	 */
	public DeclaredEntry createDeclaration(TId tnode, Node ctx, boolean allowOverloading)
	throws Exception {
		NameScope enclosingScope = getCurrentNameScope();
		String name = getSymbolWrapper().getText(tnode);
		DeclaredEntry entry = new DeclaredEntry(name, enclosingScope, ctx);
		enclosingScope.addEntry(getSymbolWrapper().getLine(ctx), name, entry, allowOverloading);
		//resolveForwardReferences(entry, checker);
		return entry;
	}

	/**
	 Annotate the Id with the DeclaredEntry that defines the Id.
	 */
	public void bind(TId id, NameScope<Type, Node, TId> scopeOfUse, VisibilityChecker checker,
		DeclaredEntry<Type, Node, TId> entry) {

		if (checker != null) checker.check(scopeOfUse, entry);
		DeclaredEntry<Type, Node, TId> e = getRef(id);
		if (e == null) setRef(id, entry);
		else assertThat(e == entry, "Attempt to override a conflicting symbol entry");
	}

	public void bind(TId id, VisibilityChecker checker, DeclaredEntry entry) {
		bind(id, getCurrentNameScope(), checker, entry);
	}

	/**
	 Find the entry in the symbol table that defines the specified id path.
	 Each id in the path represents the id
	 of an enclosing context. The current scope is first searched, and if the
	 path is not found relative to the current scope, then the global scope
	 is searched. Return null if not found. This algorithm assumes that all symbols
	 are statically resolvable - i.e., the language is strongly typed.
	 The SymbolEntry of the path's last element is returned.
	 The symbol reference is also bound to (attributed with) the symbol table entry of the symbol.
	 */
	public SymbolEntry<Type, Node, TId> resolveAndBindSymbol(List<TId> path, NameScope<Type, Node, TId> scopeOfUse,
		VisibilityChecker checker)
	{
		SymbolEntry<Type, Node, TId> entry = null;
		NameScope<Type, Node, TId> context = scopeOfUse;
		int count = 0;
		TId lastId = null;

		entry = getRef(path.get(path.size()-1));
		if (entry != null) return entry;  // was already bound - return that binding.

		for (TId id : path)
		{
			lastId = id;
			count++;
			if (count == 1) {
				// Resolve the first part of the path statically.
				entry = identifySymbol(id, context, checker);
			} else {
				// See if there is an entry in the specified context, or any
				// appended ones; but do not check contexts that enclose the
				// specified context or the appended ones.
				entry = identifySymbolLocally(id, context);
			}

			if (entry == null) return null;

			if (entry instanceof DeclaredEntry)
				bind(id, context, checker, (DeclaredEntry)entry);

			// Advance to next context, if any.
			// context = any owned scope + any structured type:
			NameScope<Type, Node, TId> ownedScope = null;
			if (entry instanceof NameScopeEntry)
				ownedScope = ((NameScopeEntry)entry).getOwnedScope();
			List<NameScope<Type, Node, TId>> implScopes = entry.getImplementedNameScopes();
			if ((ownedScope != null) || (implScopes.size() > 0)) {
				List<NameScope<Type, Node, TId>> allScopes = new LinkedList<NameScope<Type, Node, TId>>();
				allScopes.add(ownedScope);
				allScopes.addAll(implScopes);
				context = createContext(id, entry.getEnclosingScope(), getSymbolWrapper(), allScopes);
			} else {
				break;  // no context
			}
		}

		if (count < path.size()) throw new RuntimeException(
			"At line " + getSymbolWrapper().getLine(lastId) + ", '" +
			getSymbolWrapper().getText(lastId) + "' in " + createNameFromPath(path) +
			" is not the last element of the path, and it does not define a scope; " +
			"it is a " + entry.getClass().getName() +
			( entry instanceof DeclaredEntry ?
				" defined at line " + getSymbolWrapper().getLine(((DeclaredEntry<Type, Node, TId>)entry).getDefiningNode())
				: "" ));
		else
			return entry;
	}

	public SymbolEntry<Type, Node, TId> resolveAndBindSymbol(List<TId> path, VisibilityChecker checker)
	{
		return resolveAndBindSymbol(path, getCurrentNameScope(), checker);
	}

	/**
	 Combine the specified Name Scopes into one logical Name Scope.
	 */
	protected NameScope<Type, Node, TId> createContext(Node nodeThatDefinesScope,
		NameScope parentScope, SymbolWrapper<Node> symbolWrapper, List<NameScope<Type, Node, TId>> scopes) {

		/*
		Create an empty symbol table that links to each of the scopes, link those tables,
		and create a wrapper name scope for the first of the tables.
		*/
		NameScope<Type, Node, TId> nameScopeWrapper = new NameScope<Type, Node, TId>(
			nodeThatDefinesScope, parentScope, getSymbolWrapper());
		SymbolTable<Type, Node, TId> table = nameScopeWrapper.getSymbolTable();
		for (NameScope<Type, Node, TId> scope : scopes) {

			SymbolTable<Type, Node, TId> newSymbolTable = new SymbolTable<Type, Node, TId>(
				scope, parentScope.getSymbolTable(), null, getSymbolWrapper());
			newSymbolTable.appendTable(scope.getSymbolTable());
			table.appendTable(newSymbolTable);
			table = newSymbolTable;
		}
		return nameScopeWrapper;
	}


	/* Symbol identification */


	public SymbolEntry<Type, Node, TId> identifySymbol(String name, NameScope initialScope, VisibilityChecker checker)
	{
		assertThat(name != null, "symbol name is null");
		assertThat(initialScope != null, "initial scope is null");

		for (NameScope scope = initialScope; scope != null; scope = scope.getParentNameScope())
		{
			SymbolEntry<Type, Node, TId> entry = identifySymbolLocally(name, scope);
			if (entry == null) continue;
			if (checker.isVisible(scope, entry)) return entry;
		}
		return null;
	}

	/**
	 Same as identifySymbol, but don't stop after the first match; return all matches.
	 If no matches, return an empty list.
	 */
	public List<SymbolEntry<Type, Node, TId>> identifySymbols(String name, NameScope initialScope, VisibilityChecker checker)
	{
		assertThat(name != null, "symbol name is null");
		assertThat(initialScope != null, "initial scope is null");

		List<SymbolEntry<Type, Node, TId>> entries = new LinkedList<SymbolEntry<Type, Node, TId>>();

		for (NameScope scope = initialScope; scope != null; scope = scope.getParentNameScope())
		{
			List<SymbolEntry<Type, Node, TId>> foundEntries = identifySymbolsLocally(name, scope);
			if (foundEntries == null) continue;

			for (SymbolEntry<Type, Node, TId> entry : foundEntries) {
				if (checker.isVisible(scope, entry)) entries.add(entry);
			}
		}
		return entries;
	}

	public List<SymbolEntry<Type, Node, TId>> identifySymbols(TId id, NameScope initialScope, VisibilityChecker checker)
	{
		String name = getSymbolWrapper().getText(id);
		return identifySymbols(name, initialScope, checker);
	}

	/**

	 */
	public SymbolEntry<Type, Node, TId> identifySymbol(TId id, NameScope scope, VisibilityChecker checker)
	{
		// If the id already has a ref attribute, return it.
		DeclaredEntry<Type, Node, TId> ref = state.getRef(id);
		if ((ref != null) && (checker != null) && checker.isVisible(scope, ref)) return ref;

		// Search from the current scope.
		String name = getSymbolWrapper().getText(id);
		return identifySymbol(name, scope, checker);
	}

	public SymbolEntry<Type, Node, TId> identifySymbol(TId id, VisibilityChecker checker)
	{
		return identifySymbol(id, getCurrentNameScope(), checker);
	}

	public SymbolEntry<Type, Node, TId> identifySymbol(String name, VisibilityChecker checker)
	{
		return identifySymbol(name, getCurrentNameScope(), checker);
	}

	/**
	 Return the local symbol table definition for the specified symbol, if a
	 local definition exists. Also search any appended tables. Do NOT search enclosing namespaces.
	 */
	public SymbolEntry<Type, Node, TId> identifySymbolLocally(TId id, NameScope<Type, Node, TId> scope) {
		return identifySymbolLocally(getSymbolWrapper().getText(id), scope);
	}

	public SymbolEntry<Type, Node, TId> identifySymbolLocally(TId id) {
		return identifySymbolLocally(id, getCurrentNameScope());
	}

	public SymbolEntry<Type, Node, TId> identifySymbolLocally(String name, NameScope<Type, Node, TId> scope) {

		for (SymbolTable<Type, Node, TId> table = scope.getSymbolTable();
			table != null;
			table = table.getNextTable()) {

			SymbolEntry<Type, Node, TId> entry = table.getEntry(name);
			if (entry != null) return entry;
		}
		return null;
	}

	public SymbolEntry<Type, Node, TId> identifySymbolLocally(String name) {
		return identifySymbolLocally(name, getCurrentNameScope());
	}

	public List<SymbolEntry<Type, Node, TId>> identifySymbolsLocally(String name, NameScope<Type, Node, TId> scope) {

		List<SymbolEntry<Type, Node, TId>> entries = new LinkedList<SymbolEntry<Type, Node, TId>>();
		for (SymbolTable<Type, Node, TId> table = scope.getSymbolTable();
			table != null;
			table = table.getNextTable()) {

			List<SymbolEntry<Type, Node, TId>> tableEntries = table.getEntries(name);
			if (tableEntries == null) continue;

			entries.addAll(tableEntries);
		}
		return entries;
	}

	/**
	 Beginning with the current scope, and progressing outward, attempt to
	 resolve the unresolved references that are attached to each scope.
	 This method should be called after each name declaration is processed,
	 so that references to that name are then resolved.
	 */
	public void resolveForwardReferences(DeclaredEntry<Type, Node, TId> entry, VisibilityChecker checker)
	{
		for (NameScope<Type, Node, TId> scope = getCurrentNameScope();
			scope != null; scope = scope.getParentNameScope())
		{
			scope.resolveForwardReferences(entry, checker);
		}
	}


	/* Methods for annotating */

	/**
	 Instead of using the name of id, use the specified name instead.
	 */
	public SymbolEntry<Type, Node, TId> addSymbolEntry(SymbolEntry<Type, Node, TId> entry,
		TId id, String name, NameScope<Type, Node, TId> enclosingScope, boolean allowOverloading)
	throws SymbolEntryPresent {
		enclosingScope.getSymbolTable().addEntry(getSymbolWrapper().getLine(id), name, entry, allowOverloading);
		if (entry instanceof DeclaredEntry) {
			this.setDef(id, (DeclaredEntry)entry);
		}
		return entry;
	}

	public SymbolEntry<Type, Node, TId> addSymbolEntry(SymbolEntry<Type, Node, TId> entry, TId id,
		NameScope enclosingScope, boolean allowOverloading)
	throws SymbolEntryPresent
	{
		return addSymbolEntry(entry, id, this.state.getSymbolWrapper().getText(id), enclosingScope, allowOverloading);
	}

	public SymbolEntry<Type, Node, TId> addSymbolEntry(SymbolEntry<Type, Node, TId> entry, TId id,
		boolean allowOverloading)
	throws SymbolEntryPresent
	{
		return addSymbolEntry(entry, id, this.state.getSymbolWrapper().getText(id), getCurrentNameScope(), allowOverloading);
	}

	public SymbolEntry<Type, Node, TId> getIdBinding(TId id)
	{
		return this.state.getIdBinding(id);
	}

	/**
	 Create a NameScope Annotation for the specified name.
	 name may be null.
	 */
	public NameScope<Type, Node, TId> createNameScope(String name, Node node)
	{
		NameScope<Type, Node, TId> newScope = new NameScope<Type, Node, TId>(name, node,
			getCurrentNameScope(), this.getSymbolWrapper());
		this.setScope(node, pushNameScope(newScope));
		return newScope;
	}

	public NameScope<Type, Node, TId> createNameScope(Node node)
	{
		String name = null;
		return createNameScope(name, node);
	}

	/**
	 For declarations that define both an Id and a NameScope.
	 id may be null.
	 If id is not null, then a self entry is made in the parent name scope.
	 */
	public NameScope<Type, Node, TId> createNameScope(TId id, Node node, boolean allowOverloading)
	{
		String name = null;
		if (id != null) name = this.getSymbolWrapper().getText(id);
		NameScope<Type, Node, TId> newNameScope = new NameScope(name,
			node, getCurrentNameScope(), this.getSymbolWrapper());
		if (name != null) try
		{
			NameScope<Type, Node, TId> curScope = getCurrentNameScope();
			if (curScope == null) System.err.println("Warning - curScope is null");  // debug
			//if (curScope.getSymbolTable().getName() == null) System.err.println("Null named symbol table"); // debug
			NameScopeEntry nameScopeEntry = new NameScopeEntry(newNameScope, name, curScope);
			addSymbolEntry(nameScopeEntry, id, curScope, allowOverloading);
			newNameScope.setSelfEntry(nameScopeEntry);
			//resolveForwardReferences(nameScopeEntry, checker);
		}
		catch (SymbolEntryPresent ex) { throw new RuntimeException(ex); }
		this.setScope(node, pushNameScope(newNameScope));  // annotates node with the name scope
		return newNameScope;
	}

	public NameScope<Type, Node, TId> createNameScope() {
		Node node = getPendingScopeDefiner();
		TId name = popScopeName();

		// push new name scope
		if (name == null)
			return createNameScope(node);
		else
			return createNameScope(this.state.getSymbolWrapper().getText(name), node);
	}

	public NameScope<Type, Node, TId> getNameScope(Node node)
	{
		return this.state.getScope(node);
	}

	/**
	 An expression whose value is defined in the declaration of a symbol.
	 */
	public ExprAnnotation<Type, Node> setExprAnnotation(Node node, Object value, TypeDescriptor typeDesc)
	{
		return getState().setExprAnnotation(node, value, typeDesc);
	}

	public ExprAnnotation<Type, Node> getExprAnnotation(Node node) {
		return getState().getExprAnnotation(node);
	}

	public String createNameFromPath(List<TId> path) {
		String name = "";
		boolean firstTime = true;
		for (TId id : path) {
			if (firstTime) firstTime = false;
			else name = name + ".";
			name = name + getSymbolWrapper().getText(id);
		}
		return name;
	}
}
