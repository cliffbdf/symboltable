package cliffberg.symboltable;

import cliffberg.utilities.AssertionUtilities;

import java.util.List;

/**
 Implement this to describe the various simple and complex types that values
 may have in the language.
 */
public interface TypeDescriptor<Type extends ValueType> extends Annotation {

	Type getType();

	/** Create a deep copy of this type descriptor. Shallow copies are permitted
	 if it can be guaranteed that fields of the copy will not be altered.
	 */
	TypeDescriptor<Type> duplicate();

	/** Return true if (and only if) something described by this type descriptor
	 can be treated as something that is described by the specified type descriptor.
	 */
	boolean isA(TypeDescriptor<? extends Type> t);

	/** Return trure if (and only if) something of this type can be assigned to
	 something of the specified other type.
	 */
	boolean canBeAssignedTo(TypeDescriptor<? extends Type> otherType);
}
