package cliffberg.symboltable;

public class SymbolEntryPresent extends Exception
{
	public SymbolEntryPresent(int curLineNo, String name) {
		super("At line " + curLineNo + ", there is already an entry defined with name " + name);
	}
	
	public SymbolEntryPresent(int curLineNo, String name, int lineNo) {
		super("At line " + curLineNo + ", there is already an entry defined with name " + name +
			", defined at line " + lineNo);
	}
}
