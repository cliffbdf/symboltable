package cliffberg.symboltable;

import java.util.List;

/**
 An IdentHandler is attached to enclosing scopes when a symbol is not recognized
 but might be defined later in an enclosing scope. Later, when the symbol is
 defined, attached Handlers are checked to see if any refer to the symbol. If so,
 the Handler's bindRetroactively method is called, to resolve the original
 symbol reference.
 */
public abstract class IdentHandler<Node, TId>
{
	private NameResolution nameResolver;
	private SymbolWrapper symbolWrapper;
	private TId id;
	private Node definingNode;  // may be null
	private NameScope originalScope;
	private boolean orig = false;  // true if this handler is for the first appearance of
		// the identifier within its scope
	private boolean couldServeAsDecl = false;
	
	public IdentHandler(NameResolution nameResolver, SymbolWrapper symbolWrapper,
		TId id, Node definingNode, NameScope scope)
	{
		this.nameResolver = nameResolver;
		this.id = id;
		this.definingNode = definingNode;
		//if (path.size() == 0) throw new RuntimeException("Unexpected");
		this.originalScope = scope;
		attachToEnclosingScopes();
	}
	
	/**
	 This method is expected to perform all reference fixing.
	 */
	public abstract void bindRetroactively(DeclaredEntry entry);
	
	public TId getId() { return id; }
	
	public String getText() { return symbolWrapper.getText(id); }
	
	/** May be null */
	public Node getDefiningNode() { return definingNode; }
	
	public NameScope getOriginalScope() { return originalScope; }
	
	//public TId getId() { return path.get(path.size()-1); }
	
	public void setOriginal() { this.orig = true; }
	
	public boolean isOriginal() { return this.orig; }
	
	public void setCouldServeAsDeclaration() { this.couldServeAsDecl = true; }
	
	public boolean couldServeAsDeclaration() { return this.couldServeAsDecl; }
	
	/**
	 If the path is now resolvable, from its original context (scope), then perform
	 the required actions to hook up prior references to the newly defined
	 symbol. This method should be called at the end of each NameResolution method
	 that defines a new symbol (creates a new SymbolTable entry) for all of
	 its attached IdentHandlers.
	 */
	public boolean checkForPathResolution(DeclaredEntry entry, VisibilityChecker checker)
	{
		if (this.nameResolver.identifySymbol(id, this.originalScope, checker) != null)
		{
			bindRetroactively(entry);
			removeFromAllScopes();
			return true;
		}
		return false;
	}
	
	/**
	 Attach this IdentHandler to all the NameScopes that enclose the caller.
	 */
	protected void attachToEnclosingScopes()
	{
		for (NameScope scope = this.originalScope;
			scope != null;
			scope = scope.getParentNameScope())
		{
			scope.addIdentHandler(getId(), this);
		}
		nameResolver.addIdentHandler(this);
	}
	
	/**
	 Remove this IdentHandler from all NameScopes to which it is attached.
	 */
	protected void removeFromAllScopes()
	{
		for (NameScope scope = this.originalScope;
			scope != null;
			scope = scope.getParentNameScope())
		{
			scope.removeIdentHandler(getId());
		}
		
		nameResolver.removeIdentHandler(this);
	}
	
	/**
	 To be called after all symbols have been identified. Called only on
	 the remaining IdentHandlers.
	 */
	public void failure()
	{
		//TId id = path.get(0);
		throw new RuntimeException(
			"Unrecognized identifier: " + id +
				", at line " + symbolWrapper.getLine(id) + ", col " + symbolWrapper.getPos(id)
			);
	}
}
