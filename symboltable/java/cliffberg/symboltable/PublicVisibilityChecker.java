package cliffberg.symboltable;

import java.util.List;

/**
 A callback handler for checking if access is allowed to an element.
 */
public class PublicVisibilityChecker<Node, TId extends Node> implements VisibilityChecker {
	
	public PublicVisibilityChecker(Class namespaceClass) {
		this.path = path;
		this.namespaceClass = namespaceClass;
	}
	
	private List<TId> path;
	private Class namespaceClass;
	
	public void check(NameScope refScope, SymbolEntry entry) {
		if (! isVisible(refScope, entry)) {
			NameScope referringNameScope = getNamespaceNameScope(refScope);
			NameScope entryNamespaceScope = getNamespaceNameScope(entry.getEnclosingScope());
			throw new RuntimeException(
				"Element " + entry.getName() +
					" is referenced from " + referringNameScope.getName() + 
					" but is not public in " + entryNamespaceScope.getName());
		}
	}
	
	public boolean isVisible(NameScope refScope, SymbolEntry entry) {
		
		NameScope referringNameScope = getNamespaceNameScope(refScope);
		NameScope entryNamespaceScope = getNamespaceNameScope(entry.getEnclosingScope());
		if (referringNameScope != entryNamespaceScope)
			// referring scope is in a different namespace than entry name scope
			if (! entry.isDeclaredPublic()) return false;
		return true;
	}
    
	/**
	 Determine the name scope of the namespace that encloses the specified scope.
	 */
    public NameScope getNamespaceNameScope(NameScope scope) {
    	
    	for (NameScope s = scope;;) {
    		Node node = (Node)(s.getNodeThatDefinesScope());
    		if (namespaceClass.isAssignableFrom(node.getClass())) return s;
    		s = s.getParentNameScope();
    		if (s == null) break;
    	}
    	System.out.println("Namespace for scope " + scope.getName() + " not found");
    	scope.printUpward(System.err);  // debug
    	throw new RuntimeException("Namespace for scope " + scope.getName() + " not found");
    }
}
