package cliffberg.symboltable;

import java.io.Reader;
import java.io.PushbackReader;

/**
 Parses and analyzes source code. The source code is provided as input.
 */
public abstract class NamespaceProcessorBase {
	
	private Analyzer analyzer;
	private boolean prettyPrint = false;
	
	public NamespaceProcessorBase(Analyzer analyzer) {
		this.analyzer = analyzer;
	}
	
	public Analyzer getAnalyzer() { return analyzer; }
	
	public void setPrettyPrint(boolean yes) {
		this.prettyPrint = yes;
	}
	
	public boolean getPrettyPrint() {
		return this.prettyPrint;
	}
	
	/**
	 
	 */
	public NameScope processPrimaryNamespace(Reader reader, boolean parseOnly) throws Exception {
		
		NameScope nameScope = processNamespace(reader, parseOnly);
		if (parseOnly) return null;
		
		this.analyzer.getState().setPrimaryNamespaceSymbolEntry(analyzer.getEnclosingScopeEntry());
		return nameScope;
	}
	
	/**
	 Implement this to create a lexer and parser, parse the input, perform
	 analysis on the resulting tree, and then return a NameScope.
	 */
	public abstract NameScope processNamespace(Reader reader, boolean parseOnly) throws Exception;
}
