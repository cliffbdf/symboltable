package cliffberg.symboltable;

public class ConversionException extends Exception {
	public ConversionException(ValueType fromType, ValueType toType) {
		super("Cannot convert " + fromType + " to " + toType);
	}
}
